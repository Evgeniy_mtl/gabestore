<#import "parts/common.ftl" as c>
<@c.page>
<h3>Add new game</h3>
<form method="post">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Name :</label>
        <div class="col-sm-6">
            <input type="text" name="name" class="form-control" placeholder="Name of the game" >
          </div>
      </div>

      <input type="hidden" name="_csrf" value="${_csrf.token}" />
      <button class="btn btn-primary" type="submit" required>Save</button>
  </form>

  </@c.page>