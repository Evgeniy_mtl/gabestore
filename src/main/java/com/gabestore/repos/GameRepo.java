package com.gabestore.repos;

import com.gabestore.domain.Game;
import com.gabestore.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface GameRepo extends CrudRepository<Game, Long> {
    Game findByName(String name);
}