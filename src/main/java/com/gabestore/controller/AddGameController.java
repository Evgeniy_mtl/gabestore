package com.gabestore.controller;

import com.gabestore.domain.Game;
import com.gabestore.repos.GameRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class AddGameController {

    @Autowired
    private GameRepo gameRepo;

    @GetMapping("/addGame")
    public String addNewGame(){
        return "addGame";
    }

    @PostMapping("/addGame")
    public String add(@RequestParam String name) {

        Game gameFromDb = gameRepo.findByName(name);
        if (gameFromDb != null){
            return "addGame";
        }

        Game game = new Game(name);


        gameRepo.save(game);

        return "main";
    }
    /*public String addGame(Game game, Map<String, Object> model){
        Game gameFromDb = gameRepo.findByName(game.getName());

        if (gameFromDb != null){
            model.put("message","Game exists!");
            return "addGame";
        }
*/
        //game.setName();

        /*user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        userRepo.save(user);
        */

}
