package com.gabestore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GameController {

    @GetMapping("/games/metro")
    public String metro (){
        return "games/metro";
    }

    @GetMapping("/games/fallout4")
    public String fallout4 (){
        return "games/fallout4";
    }

    @GetMapping("/games/fcND")
    public String fcND (){
        return "games/fcND";
    }

    @GetMapping("/games/gta5")
    public String gta5 (){
        return "games/gta5";
    }

    @GetMapping("/games/kcd")
    public String kcd (){
        return "games/kcd";
    }

    @GetMapping("/games/ds")
    public String ds3 (){
        return "games/ds";
    }

    @GetMapping("/games/mkx")
    public String mkx (){
        return "games/mkx";
    }

    @GetMapping("/games/skyrim")
    public String skyrim (){
        return "games/skyrim";
    }

    @GetMapping("/games/re2")
    public String re2 (){
        return "games/re2";
    }


}
